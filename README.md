# DemoProject
  - `dotnet run --project ./src/DemoProject/DemoProject.csproj` 
  - Runs on localhost port 1612
  - Swagger UI available on ./swagger_ui route
  - Api Key for Generate endpoint demo: `MTY2MjhiMDgtZjQxZS00NWRlLThhYWQtN2RjN2I1ZGFjY2Jj`
  - Publicly available at `http://34.241.225.225/swagger_ui` hosted on AWS Fargate. (taken down at 4pm 28th Feb)

### Requirements -
  - Dotnet Core sdk - https://dotnet.microsoft.com/download/dotnet-core/2.2
  
### Assumptions -
  - The API Key required for authentication on the license key generation endpoint has been securely communicated with the intended user.
  - Secret key is a pre defined sha256 encrypted guid which has been UTF8 encoded before communicated.
  - Api request values have maximum and minimum allowed legnths and are validated as such.
  
### Request value limits -
  - End user full name to be 100 characters max, 1 character min.
  - Software package name to eb 100 characters max, 1 character min.
  - Generated license key can be max 203 charactrers, 5 characters min (based on allowable lengths of inputs).
 
### Problems
  - Due to only the end user full name being present on the validate endpoint, user will be validated for any software package. 
  - This could be prevented through the addition of the software package name on the validate endpoint and including the package name in the hash.
  - To restrict to a single install, software package name could be a software id relating to a particular install if required.
  - Validation could also be done against known software package names/ids at time of generation.