FROM microsoft/dotnet:2.2-sdk as builder
RUN mkdir -p /root/src/app/demoproject
WORKDIR /root/src/app/demoproject
COPY . .
RUN dotnet publish ./src/DemoProject/DemoProject.csproj -c Release -o published
FROM microsoft/dotnet:2.2-aspnetcore-runtime
ENV ASPNETCORE_URLS=http://+:1612
ENV ASPNETCORE_ENVIRONMENT=$Environment
WORKDIR /app
COPY --from=builder /root/src/app/demoproject/src/DemoProject/published .
EXPOSE 1612
CMD ["dotnet", "./DemoProject.dll"]