﻿namespace DemoProject.Core.Config
{
    public static class EnvironmentVariables
    {
        public static string HashingSecretKey => "DEMO_PROJECT_ENCRYPTION_SECRET";
        public static string GenerationRoleAuthHashedApiKey => "DEMO_PROJECT_AUTH_SECRET";
    }
}