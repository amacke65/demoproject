namespace DemoProject.Core.Enums
{
    public enum DocumentType
    {
        OpenApi,
        Swagger
    }
}