namespace DemoProject.Models.DTO.Request
{
    public class GenerateKeyRequest
    {
        public string EndUserFullName { get; set; }
        public string SoftwarePackageName { get; set; }

        public GenerateKeyRequest(string endUserFullName, string softwarePackageName)
        {
            EndUserFullName = endUserFullName;
            SoftwarePackageName = softwarePackageName;
        }
        
        public GenerateKeyRequest()
        {
        }
    }
}