namespace DemoProject.Models.DTO.Request
{
    public class ValidateKeyRequest
    {
        public string EndUserFullName { get; set; }
        public string SoftwareLicenseKey { get; set; }

        public ValidateKeyRequest(string endUserFullName, string softwareLicenseKey)
        {
            EndUserFullName = endUserFullName;
            SoftwareLicenseKey = softwareLicenseKey;
        }

        public ValidateKeyRequest()
        {
        }
    }
}