namespace DemoProject.Models.DTO.Response
{
    public class GenerateKeyResponse
    {
        public string LicenceKey { get; set; }
        public GenerateKeyResponse(string licenceKey)
        {
            LicenceKey = licenceKey;
        }
    }
}