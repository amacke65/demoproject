using System.ComponentModel.DataAnnotations;

namespace DemoProject.Models.DbEntities
{
    public class ApiKey
    {
        [Key]
        public string ApiKeyValue { get; set; }
        public string Role { get; set; }
    }
}