using System;
using DemoProject.Core.Config;
using DemoProject.Core.Enums;
using DemoProject.Models.DbEntities;
using Microsoft.EntityFrameworkCore;

namespace DemoProject.Storage
{
    public class DemoProjectDbContext : DbContext
    {
        public DbSet<ApiKey> ApiKeys { get; set; }
        public DemoProjectDbContext(DbContextOptions<DemoProjectDbContext> options) : base(options)
        {
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("DemoProjectInMemDb");
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApiKey>().HasData(new ApiKey
            {
                ApiKeyValue = Environment.GetEnvironmentVariable(EnvironmentVariables.GenerationRoleAuthHashedApiKey),
                Role = AuthorizationRoles.LicenceKeyGeneration.ToString()
            });
        }
    }
}