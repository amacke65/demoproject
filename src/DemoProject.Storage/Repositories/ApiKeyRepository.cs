using System.Threading.Tasks;
using DemoProject.Models;
using DemoProject.Models.DbEntities;

namespace DemoProject.Storage.Repositories
{
    public class ApiKeyRepository : IDataRepository<ApiKey>
    {
        private readonly DemoProjectDbContext _dbContext;
        
        public ApiKeyRepository(DemoProjectDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbContext.Database.EnsureCreated();
        }

        public async Task<ApiKey> GetAsync(string hashedApiKey)
        {
            return await _dbContext.FindAsync<ApiKey>(hashedApiKey);
        }
    }
}