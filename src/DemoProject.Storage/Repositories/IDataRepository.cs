using System.Threading.Tasks;

namespace DemoProject.Storage.Repositories
{
    public interface IDataRepository<TEntity>
    {
        Task<TEntity> GetAsync(string hashedApiKey);
    }
}