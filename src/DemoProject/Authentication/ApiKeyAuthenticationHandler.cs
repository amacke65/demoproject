using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using DemoProject.Services.ApiKeyValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DemoProject.Authentication
{
    public class ApiKeyAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IApiKeyValidationService _apiKeyValidationService;
        
        public ApiKeyAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock, IApiKeyValidationService apiKeyValidationService)
            : base(options, logger, encoder, clock)
        {
            _apiKeyValidationService = apiKeyValidationService;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("API_KEY"))
                return AuthenticateResult.Fail("Missing API_KEY Header");
            bool isValid;
            string role;
            try 
            {
                var apiKey = Request.Headers["API_KEY"];
                role = await _apiKeyValidationService.GetValidKeyRole(apiKey);
                isValid = !string.IsNullOrWhiteSpace(role);
            } 
            catch 
            {
                return AuthenticateResult.Fail("Invalid API_KEY Header");
            }

            if (!isValid)
                return AuthenticateResult.Fail("Invalid API_KEY Header");
                      
            var claims = new[] { 
                new Claim(ClaimTypes.Role, role),
            };
            
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}