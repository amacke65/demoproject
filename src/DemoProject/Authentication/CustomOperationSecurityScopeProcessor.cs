using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using NSwag;
using NSwag.SwaggerGeneration.Processors;
using NSwag.SwaggerGeneration.Processors.Contexts;
using NSwag.SwaggerGeneration.Processors.Security;

namespace DemoProject.Authentication
{
    public class CustomOperationSecurityScopeProcessor : OperationSecurityScopeProcessor, IOperationProcessor
    {
        private readonly string _name;
        public CustomOperationSecurityScopeProcessor(string name) : base(name)
        {
            _name = name;
        }
        
        public new Task<bool> ProcessAsync(OperationProcessorContext context)
        {
            if (context.OperationDescription.Operation.Security == null)
                context.OperationDescription.Operation.Security = new List<SwaggerSecurityRequirement>();

            var scopes = GetScopes(context.OperationDescription, context.MethodInfo).ToList();
            if (scopes.Any())
            {
                context.OperationDescription.Operation.Security.Add(new SwaggerSecurityRequirement
                {
                    { _name, scopes }
                });   
            }

            return Task.FromResult(true);
        }
        
        protected override IEnumerable<string> GetScopes(SwaggerOperationDescription operationDescription, MethodInfo methodInfo)
        {
            var allAttributes = methodInfo.GetCustomAttributes().Concat(
                methodInfo.DeclaringType.GetTypeInfo().GetCustomAttributes()).ToList();
            
            var isAnonymous = allAttributes.Any(attrib => attrib.GetType().Name == "AllowAnonymousAttribute");
            if(isAnonymous)
                return Enumerable.Empty<string>();
            
            var authorizeAttributes = allAttributes.Where(a => a.GetType().Name == "AuthorizeAttribute").ToList();
            if (!authorizeAttributes.Any())
                return Enumerable.Empty<string>();

            return authorizeAttributes
                .Select(a => (dynamic)a)
                .Where(a => a.Roles != null)
                .SelectMany(a => ((string)a.Roles).Split(','))
                .Distinct();
        }
    }
}