﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace DemoProject.Controllers.V1
{   
    [ApiVersion("1.0")]
    [ApiController]
    [Route("/health")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        [MapToApiVersion("1")]
        [Route("")]
        [AllowAnonymous]
        [Description("Health check endpoint.")]
        [SwaggerResponse(200, typeof(void), Description = "OK")]
        [SwaggerResponse(500, typeof(void), Description = "Internal Server Error")]
        public ActionResult Check()
        {
            return Ok();
        }
    }
}