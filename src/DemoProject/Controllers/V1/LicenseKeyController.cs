using System.ComponentModel;
using System.Threading.Tasks;
using DemoProject.Models.DTO.Request;
using DemoProject.Models.DTO.Response;
using DemoProject.Services.LicenseKey;
using DemoProject.Services.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;

namespace DemoProject.Controllers.V1
{  
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/V{version:apiVersion}/[controller]")]
    public class LicenseKeyController : ControllerBase
    {
        private readonly ILicenseKeyService _licenseKeyService;
        private readonly ILoggingService _loggingService;
        public LicenseKeyController(ILicenseKeyService licenseKeyService, ILoggingService loggingService)
        {
            _licenseKeyService = licenseKeyService;
            _loggingService = loggingService;
        }
        
        [HttpPost]
        [MapToApiVersion("1")]
        [Route("/generate")]
        [Authorize(Roles="LicenceKeyGeneration")]
        [Description("Generates an encrypted license key.")]
        [SwaggerResponse(200, typeof(ActionResult<GenerateKeyResponse>), Description = "Ok")]
        [SwaggerResponse(400, typeof(void), Description = "Bad Request")]
        [SwaggerResponse(401, typeof(void), Description = "Unauthorized")]
        [SwaggerResponse(500, typeof(void), Description = "Internal Server Error")]
        public async Task<ActionResult<GenerateKeyResponse>> Generate(GenerateKeyRequest request)
        {
            LogInfo("Generate request received");
            var licenceKey = await _licenseKeyService.Generate(request.EndUserFullName, request.SoftwarePackageName);
            return new GenerateKeyResponse(licenceKey);
        }
        
        [HttpPost]
        [MapToApiVersion("1")]
        [Route("/validate")]
        [AllowAnonymous]
        [Description("Validates an encrypted license key.")]
        [SwaggerResponse(204, typeof(void), Description = "Validated")]
        [SwaggerResponse(400, typeof(void), Description = "Bad Request")]
        [SwaggerResponse(404, typeof(void), Description = "Invalid License Key")]
        [SwaggerResponse(500, typeof(void), Description = "Internal Server Error")]
        public async Task<ActionResult> Validate(ValidateKeyRequest request)
        {
            LogInfo("Validate request received");
            if (await _licenseKeyService.Validate(request.EndUserFullName, request.SoftwareLicenseKey))
            {
                return NoContent();
            }

            return NotFound();
        }

        private void LogInfo(string message)
        {
            _loggingService.WriteInfo(message, "LicenseKeyController");
        }
    }
}