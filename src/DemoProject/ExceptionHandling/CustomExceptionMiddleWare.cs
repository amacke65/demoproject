using System;
using System.Net;
using System.Threading.Tasks;
using DemoProject.Core;
using DemoProject.Services.Logging;
using Microsoft.AspNetCore.Http;

namespace DemoProject.ExceptionHandling
{
    public class CustomExceptionMiddleWare
    {
        private readonly RequestDelegate _next;
        private readonly ILoggingService _logger;
 
        public CustomExceptionMiddleWare(RequestDelegate next, ILoggingService logger)
        {
            _logger = logger;
            _next = next;
        }
 
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.WriteError($"Something went wrong: {ex}", "ExceptionMiddleware");
                await HandleExceptionAsync(httpContext, ex);
            }
        }
 
        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
 
            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = "Internal Server Error."
            }.ToString());
        }
    }
}