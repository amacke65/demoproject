using System.Threading.Tasks;
using DemoProject.Models;
using DemoProject.Models.DbEntities;
using DemoProject.Services.Encode;
using DemoProject.Services.Hashing;
using DemoProject.Services.Logging;
using DemoProject.Storage;
using DemoProject.Storage.Repositories;

namespace DemoProject.Services.ApiKeyValidation
{
    public class ApiKeyValidationService : IApiKeyValidationService
    {
        private readonly IEncodingService _encodingService;
        private readonly IHashingService _hashingService;
        private readonly IDataRepository<ApiKey> _apiKeyRepository;
        private readonly ILoggingService _loggingService;
        public ApiKeyValidationService(IEncodingService encodingService, IHashingService hashingService, IDataRepository<ApiKey> apiKeyRepository, ILoggingService loggingService)
        {
            _encodingService = encodingService;
            _hashingService = hashingService;
            _apiKeyRepository = apiKeyRepository;
            _loggingService = loggingService;
        }
        
        public async Task<string> GetValidKeyRole(string secret)
        {
            try
            {
                var unencodedSecret = _encodingService.Decode(secret);
                var hashedKey = await _hashingService.HashWithSecret(unencodedSecret);
                var serverSecret = await _apiKeyRepository.GetAsync(hashedKey);
                if (serverSecret == null) return string.Empty;
                return hashedKey == serverSecret.ApiKeyValue ? serverSecret.Role : string.Empty;
            }
            catch
            {
                _loggingService.WriteWarning("Invalid API key", "ApiKeyValidationService");
                return string.Empty;
            }
        }
    }
}