using System.Threading.Tasks;

namespace DemoProject.Services.ApiKeyValidation
{
    public interface IApiKeyValidationService
    {
        Task<string> GetValidKeyRole(string secret);
    }
}