using System;
using System.Text;
using DemoProject.Services.Logging;

namespace DemoProject.Services.Encode
{
    public class EncodingService : IEncodingService
    {
        private readonly ILoggingService _loggingService;
        public EncodingService(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }
        
        public string Encode(string text)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(text);
                return Convert.ToBase64String(bytes);
            }
            catch (Exception e)
            {
                LogError("Encoding failure", e);
                throw;
            }
        }

        public string Decode(string text)
        {
            try
            {
                var bytes = Convert.FromBase64String(text);
                return Encoding.UTF8.GetString(bytes);
            }
            catch (Exception e)
            {
                LogError("Decoding failure", e);
                throw;
            }
        }
        
        private void LogError(string message, Exception e)
        {
            _loggingService.WriteError($"{message} - {e.Message}", "EncodingService");
        }
    }
}