namespace DemoProject.Services.Encode
{
    public interface IEncodingService
    {

        string Encode(string text);
        string Decode(string text);
    }
}