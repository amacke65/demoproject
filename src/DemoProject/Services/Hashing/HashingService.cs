using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DemoProject.Core.Config;
using DemoProject.Services.Logging;

namespace DemoProject.Services.Hashing
{
    public class HashingService : IHashingService
    {
        private readonly string _secret;
        private readonly ILoggingService _loggingService;
        public HashingService(ILoggingService loggingService)
        {
            _secret = Environment.GetEnvironmentVariable(EnvironmentVariables.HashingSecretKey);
            _loggingService = loggingService;
        }
        
        public async Task<string> HashWithSecret(string input)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                using (var hash = SHA256.Create())            
                {
                    var result = await Task.Run(() => hash.ComputeHash(Encoding.UTF8.GetBytes($"{input}{_secret}")));
                    foreach (var b in result)
                        stringBuilder.Append(b.ToString("x2"));
                }

                return stringBuilder.ToString();
            }
            catch (Exception e)
            {
                _loggingService.WriteError($"Hashing failure - {e.Message}", "HashingService");
                throw;
            }
        }
    }
}