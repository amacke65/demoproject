using System.Threading.Tasks;

namespace DemoProject.Services.Hashing
{
    public interface IHashingService
    {
        Task<string> HashWithSecret(string input);
    }
}