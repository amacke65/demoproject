using System.Threading.Tasks;

namespace DemoProject.Services.LicenseKey
{
    public interface ILicenseKeyService
    {
        Task<string> Generate(string endUserFullName, string softwarePackageName);
        Task<bool> Validate(string endUserFullName, string licenseKey);
    }
}