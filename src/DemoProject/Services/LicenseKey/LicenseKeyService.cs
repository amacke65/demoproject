using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DemoProject.Services.Encode;
using DemoProject.Services.Hashing;
using DemoProject.Services.Logging;
using Microsoft.Extensions.Logging;

namespace DemoProject.Services.LicenseKey
{
    public class LicenseKeyService : ILicenseKeyService
    {
        private readonly IHashingService _hashingService;
        private readonly ILoggingService _loggingService;
        private readonly IEncodingService _encodingService;
        
        public LicenseKeyService(IHashingService hashingService, IEncodingService encodingService, ILoggingService loggingService)
        {
            _encodingService = encodingService;
            _hashingService = hashingService;
            _loggingService = loggingService;
        }
        
        public async Task<string> Generate(string endUserFullName, string softwarePackageName)
        {
            try
            {
                var encodedEndUserFullName = _encodingService.Encode(endUserFullName);
                var encodedSoftwarePackageName = _encodingService.Encode(softwarePackageName);
                var encryptedEndUserFullName = await _hashingService.HashWithSecret(encodedSoftwarePackageName+encodedEndUserFullName);
                return GetLicenceKeyFormat(encryptedEndUserFullName, encodedSoftwarePackageName);
            }
            catch (Exception e)
            {
                LogError("License Key generation failed", e);
                throw;
            }
        }

        public async Task<bool> Validate(string endUserFullName, string licenseKey)
        {
            try
            {    
                if (!ValidateLicenseFormat(licenseKey, out var licenseParts)) return false;
                var encodedSoftwarePackageName = ReassembleSoftwarePackageName(licenseParts);
                var encodedEndUserFullName = _encodingService.Encode(endUserFullName);
                var encryptedEndUserFullName = await _hashingService.HashWithSecret(encodedSoftwarePackageName+encodedEndUserFullName);
                return GetLicenceKeyFormat(encryptedEndUserFullName, encodedSoftwarePackageName) == licenseKey;
            }
            catch (Exception e)
            {
                LogError("License Key validation failed", e);
                return false;
            }
        }
        
        private static string ReassembleSoftwarePackageName(IReadOnlyList<string> licenseParts)
        {
            var softwarePackageName = licenseParts[2] + licenseParts[0];
            return softwarePackageName;
        }

        private static bool ValidateLicenseFormat(string licenseKey, out string[] licenseParts)
        {
            licenseParts = licenseKey.Split("-");
            return licenseParts.Length == 4;
        }

        private static string GetLicenceKeyFormat(string encryptedEndUserFullName, string encodedSoftwarePackageName)
        {
            var middleOfPackageName = encodedSoftwarePackageName.Length / 2;
            var middleOfEncryptedEndUserFullName = encryptedEndUserFullName.Length / 2;
            return $"{encodedSoftwarePackageName.Substring(middleOfPackageName)}-{encryptedEndUserFullName.Substring(middleOfEncryptedEndUserFullName)}-{encodedSoftwarePackageName.Substring(0, middleOfPackageName)}-{encryptedEndUserFullName.Substring(0, middleOfEncryptedEndUserFullName)}";
        }
        
        private void LogError(string message, Exception e)
        {
            _loggingService.WriteError($"{message} - {e.Message}", "LicenseKeyService");
        }
    }
}