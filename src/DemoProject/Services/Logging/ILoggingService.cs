﻿namespace DemoProject.Services.Logging
{
    public interface ILoggingService
    {
        void WriteError(string message, string source);
        void WriteWarning(string message, string source);
        void WriteDebug(string message, string source);
        void WriteInfo(string message, string source);
    }
}