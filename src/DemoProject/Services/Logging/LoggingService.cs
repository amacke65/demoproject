﻿using System;

namespace DemoProject.Services.Logging
{
    public class LoggingService : ILoggingService
    {
        public void WriteError(string message, string source)
        {
            WriteMessage(message, source, "ERROR");
        }
        
        public void WriteWarning(string message, string source)
        {
            WriteMessage(message, source, "WARN"); 
        }
        
        public void WriteDebug(string message, string source)
        {
           WriteMessage(message, source, "DEBUG"); 
        }
        
        public void  WriteInfo(string message, string source)
        {
            WriteMessage(message, source, "INFO");
        }

        private static void WriteMessage(string message, string source, string level)
        {
            var id = Guid.NewGuid().ToString();
            Console.WriteLine($"{{Message: \"{message}\" Source : \"{source}\" Level: \"{level}\" Id: \"{id}\"}}");
        }
    }
}