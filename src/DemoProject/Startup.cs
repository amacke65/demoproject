﻿using System;
using System.Globalization;
using DemoProject.Authentication;
using DemoProject.Core.Config;
using DemoProject.Core.Enums;
using DemoProject.ExceptionHandling;
using DemoProject.Models.DbEntities;
using DemoProject.Services.ApiKeyValidation;
using DemoProject.Services.Encode;
using DemoProject.Services.Hashing;
using DemoProject.Services.LicenseKey;
using DemoProject.Services.Logging;
using DemoProject.Storage;
using DemoProject.Storage.Repositories;
using DemoProject.Validation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSwag;
using NSwag.AspNetCore;
using NSwag.SwaggerGeneration.Processors.Security;

namespace DemoProject
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            SetDefaultEnvironmentVariables();
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(opt =>
            {
                opt.Filters.Add(typeof(FluentValidationActionFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
              .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());
            services.AddDbContext<DemoProjectDbContext>();
            services.AddScoped<IDataRepository<ApiKey>, ApiKeyRepository>();
            services.AddScoped(typeof(IHashingService), typeof(HashingService));
            services.AddScoped(typeof(ILicenseKeyService), typeof(LicenseKeyService));
            services.AddScoped(typeof(ILoggingService), typeof(LoggingService));
            services.AddScoped(typeof(IApiKeyValidationService), typeof(ApiKeyValidationService));
            services.AddScoped(typeof(IEncodingService), typeof(EncodingService));
            
            services.AddAuthentication("ApiKeyAuthentication")
                .AddScheme<AuthenticationSchemeOptions, ApiKeyAuthenticationHandler>("ApiKeyAuthentication", null);
            
            services.AddApiVersioning(options =>
                {
                    options.AssumeDefaultVersionWhenUnspecified = true;
                    options.DefaultApiVersion = new ApiVersion(1, 0);
                    options.ApiVersionReader = new UrlSegmentApiVersionReader();
                })
                .AddMvcCore();
            services.AddVersionedApiExplorer(options =>
                {
                    options.GroupNameFormat = "VVV";
                    options.SubstituteApiVersionInUrl = true;
                });

            services.AddSwaggerDocument(config => GetDocumentSettings(config, DocumentType.Swagger, 1));
            services.AddOpenApiDocument(config => GetDocumentSettings(config, DocumentType.OpenApi, 1));
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware<CustomExceptionMiddleWare>();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseAuthentication();
            app.UseMvc();                     
            app.UseSwagger(x => x.Path = "/swagger/{documentName}/spec.json");
            app.UseSwaggerUi3(options =>
            {
                options.SwaggerRoutes.Add(GetSwaggerUiRoute(DocumentType.Swagger, 1));
                options.SwaggerRoutes.Add(GetSwaggerUiRoute(DocumentType.OpenApi, 1));
                options.Path = "/swagger_ui";
            });
        }

        private static SwaggerUi3Route GetSwaggerUiRoute(DocumentType documentType, int apiVersion)
        {
            return new SwaggerUi3Route(documentType.ToString(), $"/swagger/V{apiVersion} - DemoProject - {documentType}Spec/spec.json");
        }
        
        private static void GetDocumentSettings(SwaggerDocumentSettings config, DocumentType documentType, int apiVersion)
        {
            config.DocumentName = $"V{apiVersion} - DemoProject - {documentType}Spec";
            config.Title = $"V{apiVersion} - DemoProject : Licence Key Generation/Validation API";
            config.ApiGroupNames = new[] {apiVersion.ToString(CultureInfo.InvariantCulture)};
            
            config.DocumentProcessors.Add(new SecurityDefinitionAppender($"{AuthorizationRoles.LicenceKeyGeneration.ToString()}-Role", new SwaggerSecurityScheme
            {
                Type = SwaggerSecuritySchemeType.ApiKey,
                Name = "API_KEY",
                In = SwaggerSecurityApiKeyLocation.Header,
                Description = "API key for generate request authorization.",
            }));

            config.OperationProcessors.Add(new CustomOperationSecurityScopeProcessor($"{AuthorizationRoles.LicenceKeyGeneration.ToString()}-Role"));
            
            config.PostProcess = document =>
            {
                document.Info.Version = apiVersion.ToString(CultureInfo.InvariantCulture);
                document.Info.Title = "DemoProject : Licence Key Generation/Validation API";
                document.Info.Description = "A ASP.NET Core web API for licence key generation and validation";
                document.Info.TermsOfService = "None";
                document.Info.Contact = new SwaggerContact
                {
                    Name = "Andrew MacKenzie", Email = "am6234@gmail.com",
                };
            };
        }
        
        private static void SetDefaultEnvironmentVariables()
        {
            if (Environment.GetEnvironmentVariable(EnvironmentVariables.HashingSecretKey) == null)
            {
                Environment.SetEnvironmentVariable(EnvironmentVariables.HashingSecretKey, "LOCAL_DEV_SECRET_KEY");
            }
            
            if (Environment.GetEnvironmentVariable(EnvironmentVariables.GenerationRoleAuthHashedApiKey) == null)
            {
                Environment.SetEnvironmentVariable(EnvironmentVariables.GenerationRoleAuthHashedApiKey, "528b7e6d034092dd46a94841018f95ad6835e2d63a8da7894745384d3d581117");
            }
        }
    }
}
