using DemoProject.Models.DTO.Request;
using FluentValidation;

namespace DemoProject.Validation.RequestValidators
{
    public class GenerateKeyRequestValidator : AbstractValidator<GenerateKeyRequest>
    {
        public GenerateKeyRequestValidator()
        {
            RuleFor(m => m.EndUserFullName).NotEmpty().MinimumLength(1).MaximumLength(100);
            RuleFor(m => m.SoftwarePackageName).NotEmpty().MinimumLength(1).MaximumLength(100);
        }
    }
}