using DemoProject.Models.DTO.Request;
using FluentValidation;

namespace DemoProject.Validation.RequestValidators
{
    public class ValidateKeyRequestValidator : AbstractValidator<ValidateKeyRequest>
    {
        public ValidateKeyRequestValidator()
        {
            RuleFor(m => m.EndUserFullName).NotEmpty().MinimumLength(1).MaximumLength(100);
            RuleFor(m => m.SoftwareLicenseKey).NotEmpty().MinimumLength(5).MaximumLength(203);
        }
    }
}