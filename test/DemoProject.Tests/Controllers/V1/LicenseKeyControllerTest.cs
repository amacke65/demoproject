using System;
using System.Threading.Tasks;
using DemoProject.Controllers.V1;
using DemoProject.Models.DTO.Request;
using DemoProject.Models.DTO.Response;
using DemoProject.Services.Encode;
using DemoProject.Services.Hashing;
using DemoProject.Services.LicenseKey;
using DemoProject.Services.Logging;
using DemoProjectTests.Fakes;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace DemoProjectTests.Controllers.V1
{
    public class LicenseKeyControllerTest
    {
        private readonly LicenseKeyController _licenseKeyController;

        public LicenseKeyControllerTest()
        {
            ILoggingService loggingService = new NullLoggingService();
            IHashingService hashingService = new HashingService(loggingService);
            IEncodingService encodingService = new EncodingService(loggingService);
            ILicenseKeyService licenseKeyService = new LicenseKeyService(hashingService, encodingService, loggingService);
            _licenseKeyController = new LicenseKeyController(licenseKeyService, loggingService);
        }
        
        [Fact]
        public async Task Generate_WhenCalled_WithValidParameters_ReturnsOkResult()
        {
            var request = new GenerateKeyRequest("string", "string");
            var okResult = await _licenseKeyController.Generate(request);
                      
            Assert.IsType<GenerateKeyResponse>(okResult.Value);
        }
        
        [Fact]
        public async Task Generate_WhenCalled_WithValidParameters_ReturnsValidLicenseKey()
        {
            var request = new GenerateKeyRequest("string", "string");
            
            var result = await _licenseKeyController.Generate(request);
            var licenseKey = result.Value.LicenceKey;
            
            Assert.Equal( "aW5n-bd7e5f61ee1ab0d4335ac0172298efde-c3Ry-90023b0d6c76a7d34d590d47922be378",licenseKey);
        }
        
        [Fact]
        public async Task Validate_WhenCalled_WithValidLicenseKey_ReturnsNoContentResult()
        {
            var request = new ValidateKeyRequest("string", "aW5n-bd7e5f61ee1ab0d4335ac0172298efde-c3Ry-90023b0d6c76a7d34d590d47922be378");
            
            var noContentResult = await _licenseKeyController.Validate(request);
            
            Assert.IsType<NoContentResult>(noContentResult);
        }
        
        [Fact]
        public async Task Validate_WhenCalled_WithInValidLicenseKey_ReturnsNotFoundResult()
        {
            var request = new ValidateKeyRequest("string", "aW5n-bd7e5f61ee-ac0172298efde-c3Ry-90023b0d6c76a7-d47922be378");
            
            var notFoundResult = await _licenseKeyController.Validate(request);
            
            Assert.IsType<NotFoundResult>(notFoundResult);
        }
    }
}