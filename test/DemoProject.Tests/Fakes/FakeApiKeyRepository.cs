using System.Threading.Tasks;
using DemoProject.Core.Enums;
using DemoProject.Models.DbEntities;
using DemoProject.Storage.Repositories;

namespace DemoProjectTests.Fakes
{
    public class FakeApiKeyRepository : IDataRepository<ApiKey>
    {
        public async Task<ApiKey> GetAsync(string hashedApiKey)
        {
            const string knownKey = "15291f67d99ea7bc578c3544dadfbb991e66fa69cb36ff70fe30e798e111ff5f"; 
            if(hashedApiKey == knownKey)
                return new ApiKey
                {
                    ApiKeyValue = knownKey,
                    Role = AuthorizationRoles.LicenceKeyGeneration.ToString()
                };
            return null;
        }
    }
}