using DemoProject.Services.Logging;

namespace DemoProjectTests.Fakes
{
    public class NullLoggingService : ILoggingService
    {
        public void WriteError(string message, string source)
        {
        }

        public void WriteWarning(string message, string source)
        {
        }

        public void WriteDebug(string message, string source)
        {
        }

        public void WriteInfo(string message, string source)
        {
        }
    }
}