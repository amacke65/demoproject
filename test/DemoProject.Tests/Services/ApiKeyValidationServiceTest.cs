using System.Threading.Tasks;
using DemoProject.Core.Enums;
using DemoProject.Models.DbEntities;
using DemoProject.Services.ApiKeyValidation;
using DemoProject.Services.Encode;
using DemoProject.Services.Hashing;
using DemoProject.Services.Logging;
using DemoProject.Storage.Repositories;
using DemoProjectTests.Fakes;
using Xunit;

namespace DemoProjectTests.Services
{
    public class ApiKeyValidationServiceTest
    {
        private readonly ApiKeyValidationService _apiKeyValidationService;
        private readonly IEncodingService _encodingService;

        public ApiKeyValidationServiceTest()
        {
            ILoggingService loggingService = new NullLoggingService();
            IHashingService hashingService = new HashingService(loggingService);
            _encodingService = new EncodingService(loggingService);
            IDataRepository<ApiKey> apiKeyRepo = new FakeApiKeyRepository();
            _apiKeyValidationService = new ApiKeyValidationService(_encodingService, hashingService, apiKeyRepo, loggingService);    
        }
        
        [Fact]
        public async Task GetValidRole_WhenCalled_WithValidParameters_ReturnsRoleName()
        {
            var apiKey = _encodingService.Encode("testKey");
            
            var validRole = await _apiKeyValidationService.GetValidKeyRole(apiKey);
                      
            Assert.Equal(AuthorizationRoles.LicenceKeyGeneration.ToString(), validRole);
        }
        
        [Fact]
        public async Task GetValidRole_WhenCalled_WithInValidParameters_ReturnsEmptyString()
        {
            const string apiKey = "invalid";
            
            var validRole = await _apiKeyValidationService.GetValidKeyRole(apiKey);
                      
            Assert.Equal(string.Empty, validRole);
        }
    }
}