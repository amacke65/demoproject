using DemoProject.Services.Encode;
using DemoProject.Services.Logging;
using DemoProjectTests.Fakes;
using Xunit;

namespace DemoProjectTests.Services
{
    public class EncodingServiceTest
    {
        private readonly EncodingService _encodingService;
        
        public EncodingServiceTest()
        {
            ILoggingService loggingService = new NullLoggingService();
            _encodingService = new EncodingService(loggingService);
        }
        
        [Fact]
        public void Encode_WhenCalled_ReturnsEncodedString()
        {
            var encoded = _encodingService.Encode("testKey");
                                  
            Assert.Equal("dGVzdEtleQ==", encoded);
        }
        
        [Fact]
        public void Decode_WhenCalled_ReturnsUnencodedString()
        {
            var unencoded = _encodingService.Decode("dGVzdEtleQ==");
            
            Assert.Equal("testKey", unencoded);
        }
    }
}