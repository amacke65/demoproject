using System.Threading.Tasks;
using DemoProject.Services.Hashing;
using DemoProject.Services.Logging;
using DemoProjectTests.Fakes;
using Xunit;

namespace DemoProjectTests.Services
{
    public class HashingServiceTest
    {
        private readonly HashingService _hashingService;
        
        public HashingServiceTest()
        {
            ILoggingService loggingService = new NullLoggingService();
            _hashingService = new HashingService(loggingService);
        }
        
        [Fact]
        public async Task Encode_WhenCalled_ReturnsEncodedString()
        {
            var hashed = await _hashingService.HashWithSecret("testKey");
                                  
            Assert.Equal("15291f67d99ea7bc578c3544dadfbb991e66fa69cb36ff70fe30e798e111ff5f", hashed);
        }
    }
}