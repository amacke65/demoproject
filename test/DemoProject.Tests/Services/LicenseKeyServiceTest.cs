using System.Threading.Tasks;
using DemoProject.Models.DTO.Request;
using DemoProject.Services.Encode;
using DemoProject.Services.Hashing;
using DemoProject.Services.LicenseKey;
using DemoProject.Services.Logging;
using DemoProjectTests.Fakes;
using Xunit;

namespace DemoProjectTests.Services
{
    public class LicenseKeyServiceTest
    {
        private readonly LicenseKeyService _licenseKeyService;

        public LicenseKeyServiceTest()
        {
            ILoggingService loggingService = new NullLoggingService();
            IHashingService hashingService = new HashingService(loggingService);
            IEncodingService encodingService = new EncodingService(loggingService);
            _licenseKeyService = new LicenseKeyService(hashingService, encodingService, loggingService);
        }
        
        [Fact]
        public async Task Generate_WhenCalled_WithValidParameters_ReturnsValidLicenseKey()
        {
            const string input1 = "string";
            const string input2 = "string";
            
            var result = await _licenseKeyService.Generate(input1, input2);
    
            Assert.Equal("aW5n-bd7e5f61ee1ab0d4335ac0172298efde-c3Ry-90023b0d6c76a7d34d590d47922be378",result);
        }
        
        [Fact]
        public async Task Validate_WhenCalled_WithValidLicenseKey_ReturnsValidLicenseKey()
        {
            const string input1 = "string";
            const string input2 = "aW5n-bd7e5f61ee1ab0d4335ac0172298efde-c3Ry-90023b0d6c76a7d34d590d47922be378";
            
            var result = await _licenseKeyService.Validate(input1, input2);
    
            Assert.True(result);
        }
        
        [Fact]
        public async Task Validate_WhenCalled_WithInValidLicenseKey_ReturnsValidLicenseKey()
        {
            const string input1 = "string";
            const string input2 = "aW5n-bd7e5f61ee1ab0-335ac0172298efde-c3Ry-90023b0d6c76a7d34-590d47922be378";
            
            var result = await _licenseKeyService.Validate(input1, input2);
    
            Assert.False(result);
        }
    }
}