using DemoProject.Validation.RequestValidators;
using FluentValidation.TestHelper;
using Xunit;

namespace DemoProjectTests.Validators
{
    public class GenerateKeyRequestValidatorTest
    {
        private readonly GenerateKeyRequestValidator _validator;
        private readonly string _overMaxString;

        
        public GenerateKeyRequestValidatorTest()
        {
            _overMaxString = new string('*', 101);
            _validator = new GenerateKeyRequestValidator();   
        }
        
        [Fact]
        public void WhenEndUserFullNameNull_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.EndUserFullName, null as string);
        }
        
        [Fact]
        public void WhenEndUserFullNameTooLong_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.EndUserFullName, _overMaxString);
        }
        
        [Fact]
        public void WhenHaveEndUserFullName_ShouldHaveNoError()
        {           
            _validator.ShouldNotHaveValidationErrorFor(m => m.EndUserFullName, "string");
        }
        
        [Fact]
        public void WhenSoftwarePackageNameNull_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.SoftwarePackageName, null as string);
        }
        
        [Fact]
        public void WhenSoftwarePackageNameTooLong_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.SoftwarePackageName, _overMaxString);
        }
        
        [Fact]
        public void WhenHaveSoftwarePackageName_ShouldHaveNoError()
        {           
            _validator.ShouldNotHaveValidationErrorFor(m => m.SoftwarePackageName, "string");
        }
    }
}