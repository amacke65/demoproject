using DemoProject.Validation.RequestValidators;
using FluentValidation.TestHelper;
using Xunit;

namespace DemoProjectTests.Validators
{
    public class ValidateKeyRequestValidatorTest
    {
        private readonly ValidateKeyRequestValidator _validator;
        private readonly string _overMaxNameString;
        private readonly string _overMaxKeyString;
        
        public ValidateKeyRequestValidatorTest()
        {
            _overMaxNameString = new string('*', 101);
            _overMaxKeyString = new string('*', 204);
            _validator = new ValidateKeyRequestValidator();   
        }
        
        [Fact]
        public void WhenEndUserFullNameNull_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.EndUserFullName, null as string);
        }
        
        [Fact]
        public void WhenEndUserFullNameTooLong_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.EndUserFullName, _overMaxNameString);
        }
        
        [Fact]
        public void WhenHaveEndUserFullName_ShouldHaveNoError()
        {           
            _validator.ShouldNotHaveValidationErrorFor(m => m.EndUserFullName, "string");
        }
        
        [Fact]
        public void WhenSoftwareLicenseKeyNull_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.SoftwareLicenseKey, null as string);
        }
        
        [Fact]
        public void WhenSoftwareLicenseKeyTooLong_ShouldHaveError()
        {            
            _validator.ShouldHaveValidationErrorFor(m => m.SoftwareLicenseKey, _overMaxKeyString);
        }
        
        [Fact]
        public void WhenHaveSoftwarePackageName_ShouldHaveNoError()
        {           
            _validator.ShouldNotHaveValidationErrorFor(m => m.SoftwareLicenseKey, "string");
        }
    }
}